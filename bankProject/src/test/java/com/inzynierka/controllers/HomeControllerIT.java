package com.inzynierka.controllers;

import com.google.common.collect.ImmutableSet;
import com.inzynierka.core.model.User;
import com.inzynierka.core.model.security.Role;
import com.inzynierka.repository.PrimaryAccountRepository;
import com.inzynierka.repository.RoleRepository;
import com.inzynierka.repository.SavingsAccountRepository;
import com.inzynierka.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
public class HomeControllerIT extends WebAppIT {

    private static final String USER_NAME = "user";

    @Autowired
    private MockMvc mvc;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PrimaryAccountRepository primaryAccountRepository;

    @Autowired
    SavingsAccountRepository savingsAccountRepository;

    @Autowired
    UserRepository userRepository;

    @Before
    public void setUp(){
        saveUserRole();
    }

    @Test
    public void userShouldBeCreated() throws Exception{
        //given
        User user = buildMockUser();

        //when
        newUserIsBeingRegistered(user);

        //then
        User foundUser = userRepository.findByUsername(USER_NAME);
        assertNotNull(foundUser);
    }

    private User buildMockUser(){
        return new User.Builder()
                .withFirstName("firstName")
                .withLastName("lastName")
                .withPassword("password")
                .withEmail("email")
                .withUserName(USER_NAME)
                .withPhone("555-555-555")
                .build();
    }

    private void saveUserRole(){
        Role role = new Role.RoleBuilder()
                .withRoleId(1)
                .withName("USER")
                .build();
        roleRepository.save(role);
    }

    private void newUserIsBeingRegistered(User user) throws Exception {
        MultiValueMap<String, String> formParams = toFormParams(user, ImmutableSet.of("userId", "enabled", "primaryAccount","savingsAccount", "userRoles",
                "accountNonLocked", "accountNonExpired", "credentialsNonExpired", "authorities"));

        mvc.perform(post("/signup")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .params(formParams))
                .andDo(print());
    }
}
