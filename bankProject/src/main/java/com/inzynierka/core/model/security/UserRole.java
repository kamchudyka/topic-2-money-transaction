package com.inzynierka.core.model.security;

import com.inzynierka.core.model.User;

import javax.persistence.*;


@Entity
@Table(name="user_role")
public class UserRole {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long userRoleId;

    public UserRole(User user, Role role) {
        this.user = user;
        this.role = role;
    }


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id")
    private Role role;

    public UserRole() {}

    public long getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(long userRoleId) {
        this.userRoleId = userRoleId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }


    public static final class UserRoleBuilder {
        private long userRoleId;
        private User user;
        private Role role;

        public UserRoleBuilder() {
        }

        public static UserRoleBuilder anUserRole() {
            return new UserRoleBuilder();
        }

        public UserRoleBuilder withUserRoleId(long userRoleId) {
            this.userRoleId = userRoleId;
            return this;
        }

        public UserRoleBuilder withUser(User user) {
            this.user = user;
            return this;
        }

        public UserRoleBuilder withRole(Role role) {
            this.role = role;
            return this;
        }

        public UserRole build() {
            UserRole userRole = new UserRole();
            userRole.setUserRoleId(userRoleId);
            userRole.setUser(user);
            userRole.setRole(role);
            return userRole;
        }
    }
}
