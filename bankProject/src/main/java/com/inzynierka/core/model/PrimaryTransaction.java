package com.inzynierka.core.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class PrimaryTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Date date;
    private String description;
    private String type;
    private String status;
    private double amount;
    private BigDecimal availableBalance;

    public PrimaryTransaction() {}


    public PrimaryTransaction(Date date, String description, String type, String status, double amount, BigDecimal availableBalance, PrimaryAccount primaryAccount) {
        this.date = date;
        this.description = description;
        this.type = type;
        this.status = status;
        this.amount = amount;
        this.availableBalance = availableBalance;
        this.primaryAccount = primaryAccount;
    }

    @ManyToOne
    @JoinColumn(name = "primary_account_id")
    private PrimaryAccount primaryAccount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public BigDecimal getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }

    public PrimaryAccount getPrimaryAccount() {
        return primaryAccount;
    }

    public void setPrimaryAccount(PrimaryAccount primaryAccount) {
        this.primaryAccount = primaryAccount;
    }

    public static final class PrimaryTransactionBuilder {
        private Long id;
        private Date date;
        private String description;
        private String type;
        private String status;
        private double amount;
        private BigDecimal availableBalance;
        private PrimaryAccount primaryAccount;

        private PrimaryTransactionBuilder() {
        }

        public static PrimaryTransactionBuilder aPrimaryTransaction() {
            return new PrimaryTransactionBuilder();
        }

        public PrimaryTransactionBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public PrimaryTransactionBuilder withDate(Date date) {
            this.date = date;
            return this;
        }

        public PrimaryTransactionBuilder withDescription(String description) {
            this.description = description;
            return this;
        }

        public PrimaryTransactionBuilder withType(String type) {
            this.type = type;
            return this;
        }

        public PrimaryTransactionBuilder withStatus(String status) {
            this.status = status;
            return this;
        }

        public PrimaryTransactionBuilder withAmount(double amount) {
            this.amount = amount;
            return this;
        }

        public PrimaryTransactionBuilder withAvailableBalance(BigDecimal availableBalance) {
            this.availableBalance = availableBalance;
            return this;
        }

        public PrimaryTransactionBuilder withPrimaryAccount(PrimaryAccount primaryAccount) {
            this.primaryAccount = primaryAccount;
            return this;
        }

        public PrimaryTransaction build() {
            PrimaryTransaction primaryTransaction = new PrimaryTransaction();
            primaryTransaction.setId(id);
            primaryTransaction.setDate(date);
            primaryTransaction.setDescription(description);
            primaryTransaction.setType(type);
            primaryTransaction.setStatus(status);
            primaryTransaction.setAmount(amount);
            primaryTransaction.setAvailableBalance(availableBalance);
            primaryTransaction.setPrimaryAccount(primaryAccount);
            return primaryTransaction;
        }
    }
}
