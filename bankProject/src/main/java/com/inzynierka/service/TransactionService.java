package com.inzynierka.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.inzynierka.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inzynierka.repository.PrimaryAccountRepository;
import com.inzynierka.repository.PrimaryTransactionRepository;
import com.inzynierka.repository.SavingsAccountRepository;
import com.inzynierka.repository.SavingsTransactionRepository;
import com.inzynierka.core.model.PrimaryAccount;
import com.inzynierka.core.model.PrimaryTransaction;
import com.inzynierka.core.model.SavingsAccount;
import com.inzynierka.core.model.SavingsTransaction;
import com.inzynierka.core.model.User;

@Service
public class TransactionService {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private PrimaryTransactionRepository primaryTransactionRepository;
	
	@Autowired
	private SavingsTransactionRepository savingsTransactionRepository;
	
	@Autowired
	private PrimaryAccountRepository primaryAccountRepository;
	
	@Autowired
	private SavingsAccountRepository savingsAccountRepository;

	@Autowired
    private UserRepository userRepository;

	public List<PrimaryTransaction> findPrimaryTransactionList(String username){
        User user = userService.findByUsername(username);
        List<PrimaryTransaction> primaryTransactionList = user.getPrimaryAccount().getPrimaryTransactionList();

        return primaryTransactionList;
    }

    public List<SavingsTransaction> findSavingsTransactionList(String username) {
        User user = userService.findByUsername(username);
        List<SavingsTransaction> savingsTransactionList = user.getSavingsAccount().getSavingsTransactionList();

        return savingsTransactionList;
    }

    public void savePrimaryDepositTransaction(PrimaryTransaction primaryTransaction) {
        primaryTransactionRepository.save(primaryTransaction);
    }

    public void saveSavingsDepositTransaction(SavingsTransaction savingsTransaction) {
        savingsTransactionRepository.save(savingsTransaction);
    }
    
    public void savePrimaryWithdrawTransaction(PrimaryTransaction primaryTransaction) {
        primaryTransactionRepository.save(primaryTransaction);
    }

    public void saveSavingsWithdrawTransaction(SavingsTransaction savingsTransaction) {
        savingsTransactionRepository.save(savingsTransaction);
    }

    public void betweenAccountsTransfer(String transferFrom, String transferTo, String amount, PrimaryAccount primaryAccount, SavingsAccount savingsAccount) throws Exception {
        //sprawdz czy transferFrom=Konto główne i transferTo=Konto oszczędnościowe, jeżeli tak to:
            //wywolaj metode pomocnicza internalTransfer(amount, primaryAccount, savingsAccount, true);
        //jezeli nie to:
            //wywolaj metode pomocnicza internalTransfer(amount, primaryAccount, savingsAccount, false);
    }

    public void toSomeoneElseTransfer(User user, String accountType, String amount, PrimaryAccount primaryAccount, SavingsAccount savingsAccount) {
        if (accountType.equalsIgnoreCase("Konto główne")) {
            fromPrimaryToOther(user, amount, primaryAccount);
        } else if (accountType.equalsIgnoreCase("Konto oszczędnościowe")) {
            fromSavingsToOther(user, amount, savingsAccount);
        }
    }

    private void internalTransfer(String amount, PrimaryAccount primaryAccount, SavingsAccount savingsAccount, boolean isFromPrimary) {
	    //wywolaj metode pomocnicza changeAccountBalanceFor(amount, primaryAccount, savingsAccount, isFromPrimary);

        //Stworz nowy obiekt typu Date

        //Sprawdz czy isFromPrimary=true, jezeli tak to:
            //wywołaj metodę savePrimaryTransaction(date, "Transfer pomiędzy kontami", "Wewnętrzny", "Ukończony", amount, primaryAccount);
        //Jezeli nie to:
            ////wywołaj metodę savePrimaryTransaction(date, "Transfer pomiędzy kontami", "Wewnętrzny", "Ukończony", amount, savingsAccount);
    }

    private void savePrimaryTransaction(Date date, String description, String type, String status, String amount, PrimaryAccount primaryAccount){
        PrimaryTransaction primaryTransaction = new PrimaryTransaction(date, description, type, status, Double.parseDouble(amount), primaryAccount.getAccountBalance(), primaryAccount);
        primaryTransactionRepository.save(primaryTransaction);
    }

    private void saveSavingsTransaction(Date date, String description, String type, String status, String amount, SavingsAccount savingsAccount){
        SavingsTransaction savingsTransaction = new SavingsTransaction(date, description, type, status, Double.parseDouble(amount), savingsAccount.getAccountBalance(), savingsAccount);
        savingsTransactionRepository.save(savingsTransaction);
    }

    private void changeAccountBalanceFor(String amount, PrimaryAccount primaryAccount, SavingsAccount savingsAccount, boolean isFromPrimary) {
	    //Sprawdź czy isFromPrimary=true, jeżeli tak to:
            //ustaw accountBalance dla konta primary równy: od funduszy z primaryAccount odejmij amount
            //ustaw accountBalance dla konta savings równy: do funduszy z savingsAccount dodaj amount
        //Jeżeli nie to:
            //odwrotnie niż w pierwszym sprawdzeniu warunku

        //zapisz konto primaryAccount za pomoca primaryAccountRepository
        //zapisz konto savingsAccount za pomoca savingsAccountRepository
    }

    private void fromSavingsToOther(User userWhichReceive, String amount, SavingsAccount savingsAccount) {
        savingsAccount.setAccountBalance(savingsAccount.getAccountBalance().subtract(new BigDecimal(amount)));
        savingsAccountRepository.save(savingsAccount);

        updateUserWhichReceiveAccountBalance(userWhichReceive, amount);

        Date date = new Date();

        SavingsTransaction savingsTransaction = new SavingsTransaction(date, "Przelew zewnętrzny", "Zewnętrzny", "Ukończony", Double.parseDouble(amount), savingsAccount.getAccountBalance(), savingsAccount);
        savingsTransactionRepository.save(savingsTransaction);
    }

    private void fromPrimaryToOther(User userWhichReceive, String amount, PrimaryAccount primaryAccount) {
        primaryAccount.setAccountBalance(primaryAccount.getAccountBalance().subtract(new BigDecimal(amount)));
        primaryAccountRepository.save(primaryAccount);

        updateUserWhichReceiveAccountBalance(userWhichReceive, amount);

        Date date = new Date();

        PrimaryTransaction primaryTransaction = new PrimaryTransaction(date, "Przelew zewnętrzny", "Zewnętrzny", "Ukończony", Double.parseDouble(amount), primaryAccount.getAccountBalance(), primaryAccount);
        primaryTransactionRepository.save(primaryTransaction);
    }

    private void updateUserWhichReceiveAccountBalance(User userWhichReceive, String amount) {
        userWhichReceive.getPrimaryAccount().setAccountBalance(userWhichReceive.getPrimaryAccount().getAccountBalance().add(new BigDecimal(amount)));
        userRepository.save(userWhichReceive);
    }
}
