package com.inzynierka.repository;

import com.inzynierka.core.model.SavingsAccount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SavingsAccountRepository extends JpaRepository<SavingsAccount, Long> {

    SavingsAccount findByAccountNumber (int accountNumber);
}
