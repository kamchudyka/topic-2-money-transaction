package com.inzynierka.repository;

import com.inzynierka.core.model.PrimaryTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PrimaryTransactionRepository extends JpaRepository<PrimaryTransaction, Long> {

    List<PrimaryTransaction> findAll();
}
